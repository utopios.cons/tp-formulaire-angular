import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Prospect } from 'src/app/model/prospect';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  prospectForm: FormGroup;
  titre: string = "Home";
  id: number;
  prospect: Prospect = new Prospect();
  liste: Prospect[] = [];
  infos: any;
  coordonneeIsCompleted: string = "coordonneeIsCompleted";
  isCoordonnee: string = "isCoordonnee";
  stop: boolean = false;
  users: any[];

  constructor(private router: Router,
    private fomrBuild: FormBuilder, private headerService: HeaderService) {

  }
  ngOnInit(): void {





    this.headerService.changeTitre(this.titre);

    this.prospectForm = this.fomrBuild.group({
      civilite: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      mail: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      phone: ['', [Validators.required, Validators.pattern(/^(33|0)(6|7|9|3|1|2|3|4|5)\d{8}$/)]],
      ville: ['', Validators.required],
      code: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      condition: ['', Validators.required],
      promo: [''],
      contactConseiller: ['']
    })


  }

  get nom() {
    return this.prospectForm.get('nom');
  }
  get mail() {
    return this.prospectForm.get('mail');
  }
  get prenom() {
    return this.prospectForm.get('prenom');
  }
  get phone() {
    return this.prospectForm.get('phone');
  }
  get ville() {
    return this.prospectForm.get('ville');
  }
  get code() {
    return this.prospectForm.get('code');
  }
  get condition() {
    return this.prospectForm.get('condition');
  }


  return() {
    this.router.navigate(['/']);
  }


  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }


  onSubmit() {
    if (this.prospectForm.valid) {
      this.prospect = this.prospectForm.value;

      console.log(this.prospect);
      if (JSON.parse(localStorage.getItem('prospect')) == null) {

        this.liste.push(this.prospect)
        localStorage.setItem('prospect', JSON.stringify(this.liste))

      } else {

        this.liste = JSON.parse(localStorage.getItem('prospect'))
        this.liste.push(this.prospect);
        localStorage.setItem('prospect', JSON.stringify(this.liste))

      }
      this.router.navigate(['resultat']);
    } else {
      this.reloadCurrentRoute();
    }
  }
}
