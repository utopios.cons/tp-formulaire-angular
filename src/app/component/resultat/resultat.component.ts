import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Prospect } from 'src/app/model/prospect';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-resultat',
  templateUrl: './resultat.component.html',
  styleUrls: ['./resultat.component.css']
})
export class ResultatComponent implements OnInit {


  titre: string = "Résultat"

  liste: Prospect[] = JSON.parse(localStorage.getItem('prospect'))


  constructor(private router: Router, private headerService: HeaderService) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    console.log(this.liste);
  }

  navigate() {
    this.router.navigate(["/home"]);
  }

  clear() {
    localStorage.removeItem('prospect');
    this.liste = [];
  }


}
