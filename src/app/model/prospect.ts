export class Prospect {

  static count = 0;
  id: number;
  civilite: string;
  nom: string;
  prenom: string;
  mail: string;
  phone: string;
  ville: string;
  code: string;
  situationFamilliale: string;
  personneCharge: number;
  revenuRef: number;
  lieuHabitation: string;
  contactConseiller: boolean;
  promo: boolean;
  condition: boolean;

  constructor() {
    this.id = ++Prospect.count;
  }
}

